<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
</head>
<body>
<h1>
	Hello world!  
</h1>

<spring:eval expression="@applicationProps['application.version']" var="applicationVersion"/>
<spring:url value="/resources-{applicationVersion}" var="resourceUrl">
    <spring:param name="applicationVersion" value="${applicationVersion}"/>
</spring:url>

<script src="${resourceUrl}/js/app.js" type="text/javascript"> </script>

<P>  The time  on the server is ${serverTime}. </P>

<spring:url var="authUrl"
       value="/static/j_spring_security_logout" />

<p><a href = "${authUrl}"/>logout</a></p>
<p><a href = "admin">admin</a></p>
<p><a href = "operations/fully">fully</a></p>
<p><a href = "operations/authenticated">authenticated</a></p>

</body>
</html>
