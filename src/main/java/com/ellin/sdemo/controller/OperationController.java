package com.ellin.sdemo.controller;

import com.ellin.sdemo.service.domain.Meat;
import com.ellin.sdemo.service.domain.User;
import com.ellin.sdemo.service.exceptions.OutofMeatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Controller
@RequestMapping("/operations")
public class OperationController {

    @Autowired
    User user;

    @Autowired
    private com.ellin.sdemo.service.DinnerService dinnerService;


    @RequestMapping(value = {"/fully"}, method = RequestMethod.GET)
    public String mustBeLoggedInFully() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        SecurityContext secContext = SecurityContextHolder.getContext();
        String name = auth.getName(); //get logged in username
        user.setHello(name);
        user.sayHello();
        return "fully";
    }

    @RequestMapping(value = {"/authenticated"}, method = RequestMethod.GET)
    public String mustBeAuthenticated() {
        return "authenticated";
    }

    @RequestMapping(value = {"/dinner/cook/{meatQty}"}, method = RequestMethod.GET)
    public String cookMeat(@PathVariable double meatQty) throws OutofMeatException {

        System.err.println("Smeek would do this");
        dinnerService.cookMeat(meatQty);

        return "cook";
    }

    @RequestMapping(value = {"/dinner/eat/{meatQty}"}, method = RequestMethod.GET)
    public @ResponseBody Meat eatMeat(@PathVariable double meatQty) throws OutofMeatException {

        return dinnerService.eatMeat(meatQty);

    }

    @ExceptionHandler(OutofMeatException.class)
    public String handlException(HttpServletRequest request, HttpServletResponse response){
        return "outofmeat";

    }

}
