package com.ellin.sdemo.controller;

import javax.servlet.http.HttpServletRequest;

import com.ellin.sdemo.service.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

  @Autowired
  User user = null;

  @RequestMapping("/login")
  public String showLoginForm(ModelMap model, HttpServletRequest request) {

      request.getSession().invalidate();


      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	  SecurityContext secContext = SecurityContextHolder.getContext();
      secContext.setAuthentication(null);
      String name = auth.getName(); //get logged in username
      System.err.println(name);
      model.addAttribute("username", name);
      user.setHello(name);
      user.sayHello();
	  return "login";
			  
  }
  
  
  
}
