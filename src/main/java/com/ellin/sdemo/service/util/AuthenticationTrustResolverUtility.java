package com.ellin.sdemo.service.util;

import java.util.List;

import javax.servlet.Filter;

import com.ellin.sdemo.MyAuthenticationTrustResolverImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.ExceptionTranslationFilter;


/**
 * When using the http name space you can not modify the AuthenticationTrust Resolver used by the
 * ExceptionTranslationFilter. This utility will locate the filter and set it to the custom
 * AuthenticationTrustResolver
 */
public class AuthenticationTrustResolverUtility {

    @Autowired
    public AuthenticationTrustResolverUtility(FilterChainProxy etf) {
        AuthenticationTrustResolver authenticationTrustResolver = new MyAuthenticationTrustResolverImpl();

        List<SecurityFilterChain> m = etf.getFilterChains();
        for (SecurityFilterChain f : m) {
            for (Filter filter : f.getFilters()) {
                if (filter instanceof ExceptionTranslationFilter) {
                    ExceptionTranslationFilter etFilter = (ExceptionTranslationFilter) filter;
                    etFilter.setAuthenticationTrustResolver(authenticationTrustResolver);
                }

            }

        }
    }

}
