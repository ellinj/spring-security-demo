package com.ellin.sdemo.service.domain;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created with IntelliJ IDEA.
 * User: jellin
 * Date: 5/23/13
 * Time: 2:11 PM
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement
public class Meat {

    double lbs = 0;

    public Meat() {
    }

    public Meat(double lbs) {
        this.lbs = lbs;
    }

    public double getLbs() {
        return lbs;
    }

    public void setLbs(double lbs) {
        this.lbs = lbs;
    }
}
