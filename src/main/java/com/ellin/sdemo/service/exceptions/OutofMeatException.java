package com.ellin.sdemo.service.exceptions;

/**
 * Created with IntelliJ IDEA.
 * User: jellin
 * Date: 5/23/13
 * Time: 2:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class OutofMeatException extends Exception {

    public OutofMeatException(String message){

        super(message);

    }
}
