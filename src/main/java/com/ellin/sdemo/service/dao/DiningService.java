package com.ellin.sdemo.service.dao;

import com.ellin.sdemo.service.domain.Meat;
import com.ellin.sdemo.service.exceptions.OutofMeatException;

/**
 * Created with IntelliJ IDEA.
 * User: jellin
 * Date: 5/23/13
 * Time: 2:08 PM
 * To change this template use File | Settings | File Templates.
 */
public interface DiningService {


    /*
     * Cook some meat and make available for consumption
     */
    public void cookMeat(Meat meat);

    public Meat consumeMeat(double meet) throws OutofMeatException;


}
