package com.ellin.sdemo.service.dao;

import com.ellin.sdemo.service.domain.Meat;
import com.ellin.sdemo.service.exceptions.OutofMeatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: jellin
 * Date: 5/23/13
 * Time: 2:12 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class DiningServiceImpl implements DiningService {

    double availableMeat = 0;

    private static final Logger logger = LoggerFactory.getLogger(DiningServiceImpl.class);


    @Override
    public void cookMeat(Meat meat) {

        availableMeat += meat.getLbs();

    }

    @Override
    public Meat consumeMeat(double desiredMeat) throws OutofMeatException {

        logger.info("availble meat "+availableMeat);

        if (availableMeat != 0 && desiredMeat > 0) {

            if (availableMeat >= desiredMeat) {
                availableMeat -= desiredMeat;
                logger.info("availble meat "+availableMeat + " after consuming " + desiredMeat);

                return new Meat(desiredMeat);
            } else {
                Meat meet = new Meat(availableMeat);
                availableMeat = 0;
                return meet;
            }

        } else {
            throw new OutofMeatException("We are out of Meat");
        }


    }
}
