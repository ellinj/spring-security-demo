package com.ellin.sdemo.service;

import com.ellin.sdemo.service.dao.DiningService;
import com.ellin.sdemo.service.domain.Meat;
import com.ellin.sdemo.service.exceptions.OutofMeatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;

@Component
public class DinnerService {


    private static final Logger logger = LoggerFactory.getLogger(DinnerService.class);

    @Autowired
    private DiningService meatService;


	@Secured("ROLE_SMEEK")
	public void cookMeat(double meatQty) throws OutofMeatException{

           meatService.cookMeat(new Meat(meatQty));
	}

    @Secured("ROLE_SMEEK")
    public Meat eatMeat(double meatQty) throws OutofMeatException{

        return meatService.consumeMeat(meatQty);
    }

}
