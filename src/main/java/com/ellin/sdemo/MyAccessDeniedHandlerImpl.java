package com.ellin.sdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationException;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;

public class MyAccessDeniedHandlerImpl extends AccessDeniedHandlerImpl {
	
	String errorPage = "/login";
	 public static final String SPRING_SECURITY_ACCESS_DENIED_EXCEPTION_KEY = "SPRING_SECURITY_403_EXCEPTION";
     private static final Logger logger = LoggerFactory.getLogger(MyAccessDeniedHandlerImpl.class);

	    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException)
	            throws IOException, ServletException {
	        if (!response.isCommitted()) {
	            if (errorPage != null) {
	                 RequestCache requestCache = new HttpSessionRequestCache();

	                requestCache.saveRequest(request, response);
	                
	                LoginUrlAuthenticationEntryPoint luep = new LoginUrlAuthenticationEntryPoint("/login");
	                luep.commence(request, response, new RememberMeAuthenticationException("Something Happened"));

	            	/*
	                // Put exception into request scope (perhaps of use to a view)
	                request.setAttribute(WebAttributes.ACCESS_DENIED_403, accessDeniedException);

	                // Set the 403 status code.
	                response.setStatus(HttpServletResponse.SC_FORBIDDEN);

	                // forward to error page.
	                RequestDispatcher dispatcher = request.getRequestDispatcher(errorPage);
	                dispatcher.forward(request, response);*/
	            } else {
	                response.sendError(HttpServletResponse.SC_FORBIDDEN, accessDeniedException.getMessage());
	            }
	        }
	    }

}
