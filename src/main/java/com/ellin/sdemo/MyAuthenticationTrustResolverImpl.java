package com.ellin.sdemo;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;

public class MyAuthenticationTrustResolverImpl extends AuthenticationTrustResolverImpl{
	
	  private Class<? extends Authentication> anonymousClass = AnonymousAuthenticationToken.class;
	  private Class<? extends Authentication> rememberMeClass = RememberMeAuthenticationToken.class;

	
	  public boolean isAnonymous(Authentication authentication) {
	        if ((anonymousClass == null) || (authentication == null)) {
	            return false;
	        }
	        
	        if (this.isRememberMe(authentication)){
	        	return true;
	        }

	        return anonymousClass.isAssignableFrom(authentication.getClass());
	    }

}
